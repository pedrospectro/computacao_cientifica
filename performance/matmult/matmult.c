#include <stdio.h>
#include <stdlib.h>    /* exit, malloc, calloc, etc. */
#include <string.h>
#include <getopt.h>    /* getopt */
#include "matriz.h"
#include "utils.h"
#ifdef LIKWID_PERFMON
#include <likwid.h>
#else
#define LIKWID_MARKER_INIT
#define LIKWID_MARKER_THREADINIT
#define LIKWID_MARKER_SWITCH
#define LIKWID_MARKER_REGISTER(regionTag)
#define LIKWID_MARKER_START(regionTag)
#define LIKWID_MARKER_STOP(regionTag)
#define LIKWID_MARKER_CLOSE
#define LIKWID_MARKER_GET(regionTag, nevents, events, time, count)
#endif

/**
 * Exibe mensagem de erro indicando forma de uso do programa e termina
 * o programa.
 */

static void usage(char *progname)
{
  fprintf(stderr, "Forma de uso: %s [ -n <ordem> ] [ -p <passo> ] [ -m <inicio> -M <fim> ] \n", progname);
  exit(1);
}



/**
 * Programa principal
 * Forma de uso: matmult [ -n <ordem> ] [ -p <passo> ] [ -m <inicio> -M <fim> ]
 * -n <ordem>: ordem da matriz quadrada e dos vetores
 * -m <inicio> e -M <fim> : permite repetir as operações com as matrizes e vetores 
 *                          com diversas ordens, com 'inicio <= n <= fim'.
 *                          O passo entre valores de 'n' neste caso é dada pela
 *                          opção '-p <passo>' 
 *
 */

int main (int argc, char *argv[]) 
{
  LIKWID_MARKER_INIT;
  int c, passo=DEF_PASSO, n=DEF_LINS, m=DEF_MIN, M=DEF_MAX, hasM=0, hasm=0;
  double norma;
  
  MatPtr mPtr;
  MatRow mRow;
  MatCol mCol;
  Vetor vet, resPtr, resRow, resCol;
  
  /* =============== TRATAMENTO DE LINHA DE COMAANDO =============== */

  char *opts = "n:m:M:p:";
  c = getopt (argc, argv, opts);
  
  while ( c != -1 ) {
    switch (c) {
    case 'n':  n = atoi(optarg);              break;
    case 'm':  m = atoi(optarg);      hasm=1; break;
    case 'M':  M = atoi(optarg);      hasM=1; break;
    case 'p':  passo = atoi(optarg);          break;
    default:   usage(argv[0]);
    }
      
    c = getopt (argc, argv, opts);
  }

  if ( hasm != hasM)   usage(argv[0]);
  
  /* ================ FIM DO TRATAMENTO DE LINHA DE COMANDO ========= */
  
  
  srand(20182);
  
  if (!hasm && !hasM) m = M = n;

  for (n=m; n <= M; n+=passo) {
    
    mPtr = geraMatPtr (n, n);
    mRow = geraMatRow (n, n);
    mCol = geraMatCol (n, n);
    vet = geraVetor (n);
    
#ifdef DDEBUG
    prnMatPtr (mPtr, n, n);
    prnMatRow (mRow, n, n);
    prnMatCol (mCol, n, n);
    prnVetor (vet, n);
    printf ("=================================\n\n");
#endif /* DEBUG */
    double t0;
    
    resPtr = (double *) calloc (n, sizeof(double));
    LIKWID_MARKER_START("multMatPtsVet");
    t0 = timestamp();
    multMatPtrVet (mPtr, vet, n, n, resPtr);
    printf("Tempo multMatPtsVet = %lf\n", timestamp() - t0 );
    LIKWID_MARKER_STOP("multMatPtsVet");

    resPtr = (double *) calloc (n, sizeof(double));
    LIKWID_MARKER_START("multMatPtsVetCol");
    t0 = timestamp();
    multMatPtrVetCol (mPtr, vet, n, n, resPtr);
    printf("Tempo multMatPtsVetCol = %lf\n", timestamp() - t0 );
    LIKWID_MARKER_STOP("multMatPtsVetCol");



    resRow = (double *) calloc (n, sizeof(double));
    LIKWID_MARKER_START("multMatRowVet");
    t0 = timestamp();
    multMatRowVet (mRow, vet, n, n, resRow);
    printf("Tempo multMatRowVet = %lf\n", timestamp() - t0 );
    LIKWID_MARKER_STOP("multMatRowVet");
    
    resCol = (double *) calloc (n, sizeof(double));
    LIKWID_MARKER_START("multMatColVet");
    t0 = timestamp();
    multMatColVet (mCol, vet, n, n, resCol);
    printf("Tempo mutMatColVet = %lf\n", timestamp() - t0 );
    LIKWID_MARKER_STOP("multMatColVet");

    t0 = timestamp();
    norma = normaMax(resRow, resPtr, n);
    printf("Tempo normaMax = %lf\n", timestamp() - t0 );

    t0 = timestamp();
    norma = normaEucl(resCol, n);
    printf("Tempo normaEucl = %lf\n", timestamp() - t0 );

#ifdef DDEBUG
    prnVetor (resPtr, n);
    prnVetor (resRow, n);
    prnVetor (resCol, n);
#endif /* DEBUG */

  }
  LIKWID_MARKER_CLOSE;
  return 0;
}

