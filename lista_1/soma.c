#include <stdio.h>
#include <stdlib.h>

#define VALOR 0.6f
#define NUM_ELEMENTOS 10

float somaSequencia( float *dados, unsigned int tam )
{
  float soma = 0.0;
  while( tam-- ) soma += dados[tam];
  return soma;
}

float somaPar( float *dados, unsigned int tam )
{
  if (tam == 2) return dados[0] + dados[1];
  if (tam == 1) return dados[0];
  unsigned int div = tam / 2;
  return somaPar(dados, div) + somaPar(dados + div, tam - div);
}

float somaKahan( float *dados, unsigned int tam ) {
  float soma = 0.0;
  float c = 0.0;
  for(int i=0;i<tam;i++){
    float y = dados[i] - c;
    float t = soma + y;
    c = (t - soma) - y;
    soma = t;
  }
  return soma;
}

void main()
{
  float *dados = (float*) malloc(NUM_ELEMENTOS * sizeof(float));
  for (unsigned int i = 0; i < NUM_ELEMENTOS; ++i) dados[i] = VALOR;
  
  printf("NUM ELEMENTOS = %d\n", NUM_ELEMENTOS);
  float soma1 = somaSequencia(dados, NUM_ELEMENTOS);
  printf("  Soma sequencia: %1.30f\n", soma1);
  float soma2 = somaPar( dados, NUM_ELEMENTOS);
  printf("  Soma par: %1.30f\n", soma2);
  float soma3 = somaKahan( dados, NUM_ELEMENTOS);
  printf("  Soma Kahan: %1.30f\n", soma3);
  
  free (dados);
}
