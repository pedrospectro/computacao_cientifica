#include <stdio.h>
#include <stdlib.h>
#include <float.h>
#include <math.h>

#define TAM 1

double pot(double x, int e){
  if(e<1) return 1;
  return x*pot(x,e-1);
}

double fx(double *p, int n, double x){
  double fx = 0;
  for(int i=0;i<n;i++) fx += p[i]*pot(x,i);
  return fx;
}

double dfx(double *p, int n, double x){
  double dfx = 0;
  for(int i=1;i<n;i++) dfx += p[i]*i*pot(x,i-1);
  return dfx;
}

void calculaPolinomioEDerivada(
  double *p, int n, double x, double *px, double *dpx
)
{
  *px = fx(p,n,x);
  *dpx = dfx(p,n,x);
  printf("  calculaPolinomioEDerivada:\n");
  printf("    x = %f, fx = %f, dfx = %f\n",
    x,
    *px,
    *dpx
  );
}

// int mudaSinal(double *p, int n){
//   if(fx(p,n,MINR)*fx(p,n,MAXR)<0)
//     return 1;
//   return 0;
// }

int funcaoFazAlgo(
  double *p, int n, double *x, double erroMax
)
{
  double px, dpx, erro, x_new, last_erro;
  int it = 0;int teste;
  erro = 100000000000;
  do {
    it++;
    printf("\nIteracao %d:\n",it);
    calculaPolinomioEDerivada(p, n, *x, &px, &dpx);
    if (px == 0.0){
      printf("  fx atingiu valor 0\n");
      return -1;
    }
    if (n == 1){
      printf("Funcao constante nao possui raiz\n");
      return -1;
    }
    if(dpx == 0.0) dpx = FLT_EPSILON;
    x_new = *x - px/dpx;
    printf("  xnew = %f\n", x_new);
    last_erro = erro;
    erro = fabs( x_new - *x );
    printf("  erro = %f\n", erro);
    *x = x_new;
    if(last_erro < erro){
      printf("Funcao nao esta convergindo\n");
      return -1;
    }
    scanf("%d",&teste);
  } while (erro > erroMax);
  return 0;
}

int main(){
  double x = 10;
  double erroMax = 0.001;
  double *p = (double *)malloc(sizeof(double)*TAM);
  p[2] = -1; p[1] = -4; p[0] = -10;

  funcaoFazAlgo(p, TAM, &x, erroMax);
  printf("\nRESULTADO x = %f, fx = %f, dfx = %f\n",
    x,
    fx(p,TAM,x),
    dfx(p,TAM,x)
  );
}