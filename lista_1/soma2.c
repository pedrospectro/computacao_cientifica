#include <stdio.h>

int main()
{
  float soma1=0.0f, soma2=0.0f;
  float result = 1.644934;
  for (int i=1; i<=300; ++i) soma1 += 1.0f / (i*i);
  for (int i=300; i>=1; --i) soma2 += 1.0f / (i*i);
  printf("Soma1: %.10g \t Soma2: %.10g\n\n", soma1, soma2);
  printf("diffSoma1: %.10g \t diffSoma2: %.10g\n\n", result-soma1, result-soma2);
  return 0;
}