#include <stdio.h>
#include <stdint.h>
#include <float.h>
#include <math.h>

typedef union{
  int32_t i;
  float f;
  struct { // Bitfields for exploration. Do not use in production code.
    uint32_t mantissa : 23;
    uint32_t exponent : 8;
    uint32_t sign : 1;
  } parts;
} Float_t;

void printFloat( Float_t num ){
  printf(
    "f:%1.9e, ix:0x%08X, s:%d, e:%d, mx:0x%06X, relative_eps:%1.9e\n",
    num.f,
    num.i,
    num.parts.sign,
    num.parts.exponent,
    num.parts.mantissa,
    (num.f * FLT_EPSILON)
  );
}

int ulpSize(Float_t   b, Float_t a){
  printFloat(b);
  printFloat(a);
  return fabs(b.i - a.i);
}

int AlmostEqualRelative(float A, float B){
  A = fabs(A); B = fabs(B);
  float epsilon = (B > A) ? B*FLT_EPSILON : A*FLT_EPSILON;
  if(fabs(A - B) <= epsilon) return 1;
  return 0;
}

int AlmostDiffRelative(float A, float B){
  return !AlmostEqualRelative(A,B);
}

int AlmostSmallEqualRelative(float A, float B){
  if(AlmostEqualRelative(A,B)) return 1;
  A = fabs(A); B = fabs(B);
  float epsilon = (B > A) ? B*FLT_EPSILON : A*FLT_EPSILON;
  if((A - B) <= epsilon) return 1;
  return 0;
}

int AlmostSmallRelative(float A, float B){
  A = fabs(A); B = fabs(B);
  float epsilon = (B > A) ? B*FLT_EPSILON : A*FLT_EPSILON;
  if((A - B) < epsilon) return 1;
  return 0;
}

int AlmostGreaterEqualRelative(float A, float B){
  if(AlmostEqualRelative(A,B)) return 1;
  A = fabs(A); B = fabs(B);
  float epsilon = (B > A) ? B*FLT_EPSILON : A*FLT_EPSILON;
  if((A - B) >= epsilon) return 1;
  return 0;
}

int AlmostGreaterRelative(float A, float B){
  A = fabs(A); B = fabs(B);
  float epsilon = (B > A) ? B*FLT_EPSILON : A*FLT_EPSILON;
  if((A - B) > epsilon) return 1;
  return 0;
}

int AlmostEqualZero(float A){
  A = fabs(A);
  float epsilon = FLT_EPSILON;
  if(fabs(A - 0.0) <= epsilon) return 1;
  return 0;
}

int fat(int n){
  if(n <= 1) return 1;
  return n * fat(n - 1);
}

float pot(float a, int b){
  if(b <= 0) return 1.0;
  return a * pot(a, b - 1);
}

float eValue(int n){
  float e = 0;
  for(int i = 0; i < n; i++) e += (1 / (float) fat(i));
  return e;
}

float seriesMacl(int x, int n){
  float sum = 0;
  for(int i = 0; i < n; i++) sum += pot(x, i) / (float)fat(i);
  return sum;
}

int main()
{
  // float b = FLT_EPSILON;
  // float a = 1;
  // printFloat( (Float_t) a);
  // printFloat( (Float_t) b);
  // printFloat( (Float_t) (a-b));
  // printf("ulp zise = %d\n", ulpSize((Float_t) (a-b),(Float_t) a));
  // float b = 0.000000000000000000000000000000000000000000001;
  // float a = FLT_EPSILON;
  // printf("ulp zise = %d\n", ulpSize((Float_t) b,(Float_t) a));
  // printf("AlmostGreaterEqualRelative = %d\n", AlmostGreaterEqualRelative(a,b));

  

  return 0;
}