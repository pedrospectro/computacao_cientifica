#include <stdio.h>
#include <math.h>
#include <float.h>
#include <stdlib.h>

#define AMOSTR 10

double *constroiVetor(int n){ return (double*)malloc(sizeof(double) * n); }

double fxStyblinskiTang(double *x, int n){
  double sum = 0;
  for(int i = 0; i < n; i++) sum += (pow(x[i], 4) - (16 * pow(x[i], 2)) + (5 * x[i]));
  return sum / 2;
}

double xiPasso(double a, double passo, int i){ return a + (passo * i); }

/**
* Integral Trapezio multidimensional - f(x) tal que x é um vetor com cardinaliade n
**/

double calcIntegralTrapezioCompostaStyblinskiTang1(double a, double b, int intervalos, int n){
  double xi, xNext;
  double sum = 0;
  double h = (b - a) / intervalos; 
  for(int i = 0; i < (intervalos - 1); i++){
    xi = xiPasso(a, h, i);
    xNext = xiPasso(a, h, i + 1);
    sum += (xNext - xi) * ( fxStyblinskiTang((double[]){ xi }, n) + fxStyblinskiTang((double[]){ xNext }, n) );
  }
  return (sum / 2);
}

double calcIntegralTrapezioCompostaStyblinskiTang2(double a, double b, int intervalos, int n){
  double xi;
  double sum = 0;
  double h = (b - a) / intervalos; 
  for(int i = 0; i < intervalos; i++){
    xi = xiPasso(a, h, i);
    for(int j = 0; j < intervalos; j++) sum += fxStyblinskiTang((double[]){ xi, xiPasso(a, h, j) }, n);
  }
  return (pow(h, n) * sum);
}

double calcIntegralTrapezioCompostaStyblinskiTang3(double a, double b, int intervalos, int n){
  double xi, xj;
  double sum = 0;
  double h = (b - a) / intervalos; 
  for(int i = 0; i < intervalos; i++){
    xi = xiPasso(a, h, i);
    for(int j = 0; j < intervalos; j++){
      xj = xiPasso(a, h, j);
      for(int k = 0; k < intervalos; k++) sum += fxStyblinskiTang((double[]){ xi, xj ,xiPasso(a, h, k) }, n);
    }
  }
  return (pow(h, n) * sum);
}

double calcIntegralTrapezioCompostaStyblinskiTang4(double a, double b, int intervalos, int n){
  double xi, xj, xk;
  double sum = 0;
  double h = (b - a) / intervalos; 
  for(int i = 0; i < intervalos; i++){
    xi = xiPasso(a, h, i);
    for(int j = 0; j < intervalos; j++){
      xj = xiPasso(a, h, j);
      for(int k = 0; k < intervalos; k++){
        xk = xiPasso(a, h, k);
        for(int l = 0; l < intervalos; l++) sum += fxStyblinskiTang((double[]){ xi, xj, xk, xiPasso(a, h, l) }, n);
      }
    }
  }
  return (pow(h, n) * sum);
}

double calcIntegralTrapezioCompostaStyblinskiTang5(double a, double b, int intervalos, int n){
  double xi, xj, xk, xl;
  double sum = 0;
  double h = (b - a) / intervalos; 
  for(int i = 0; i < intervalos; i++){
    xi = xiPasso(a, h, i);
    for(int j = 0; j < intervalos; j++){
      xj = xiPasso(a, h, j);
      for(int k = 0; k < intervalos; k++){
        xk = xiPasso(a, h, k);
        for(int l = 0; l < intervalos; l++){
          xl = xiPasso(a, h, l);
          for(int m = 0; m < intervalos; m++)
            sum += fxStyblinskiTang((double[]){ xi, xj, xk, xl, xiPasso(a, h, m) }, n);
        }
      }
    }
  }
  return (pow(h, n) * sum);
}

double calcIntegralTrapezioCompostaStyblinskiTang6(double a, double b, int intervalos, int n){
  double xi, xj, xk, xl, xm;
  double sum = 0;
  double h = (b - a) / intervalos; 
  for(int i = 0; i < intervalos; i++){
    xi = xiPasso(a, h, i);
    for(int j = 0; j < intervalos; j++){
      xj = xiPasso(a, h, j);
      for(int k = 0; k < intervalos; k++){
        xk = xiPasso(a, h, k);
        for(int l = 0; l < intervalos; l++){
          xl = xiPasso(a, h, l);
          for(int m = 0; m < intervalos; m++){
            xm = xiPasso(a, h, m);
            for(int nn = 0; nn < intervalos; nn++)
              sum += fxStyblinskiTang((double[]){ xi, xj, xk, xl, xm, xiPasso(a, h, nn) }, n);
          }
        }
      }
    }
  }
  return (pow(h, n) * sum);
}

double calcIntegralTrapezioCompostaStyblinskiTang7(double a, double b, int intervalos, int n){
  double xi, xj, xk, xl, xm, xn;
  double sum = 0;
  double h = (b - a) / intervalos; 
  for(int i = 0; i < intervalos; i++){
    xi = xiPasso(a, h, i);
    for(int j = 0; j < intervalos; j++){
      xj = xiPasso(a, h, j);
      for(int k = 0; k < intervalos; k++){
        xk = xiPasso(a, h, k);
        for(int l = 0; l < intervalos; l++){
          xl = xiPasso(a, h, l);
          for(int m = 0; m < intervalos; m++){
            xm = xiPasso(a, h, m);
            for(int nn = 0; nn < intervalos; nn++){
              xn = xiPasso(a, h, nn);
              for(int o = 0; o < intervalos; o++)
                sum += fxStyblinskiTang((double[]){ xi, xj, xk, xl, xm, xn, xiPasso(a, h, o) }, n);
            }
          }
        }
      }
    }
  }
  return (pow(h, n) * sum);
}

double calcIntegralTrapezioCompostaStyblinskiTang8(double a, double b, int intervalos, int n){
  double xi, xj, xk, xl, xm, xn, xo;
  double sum = 0;
  double h = (b - a) / intervalos; 
  for(int i = 0; i < intervalos; i++){
    xi = xiPasso(a, h, i);
    for(int j = 0; j < intervalos; j++){
      xj = xiPasso(a, h, j);
      for(int k = 0; k < intervalos; k++){
        xk = xiPasso(a, h, k);
        for(int l = 0; l < intervalos; l++){
          xl = xiPasso(a, h, l);
          for(int m = 0; m < intervalos; m++){
            xm = xiPasso(a, h, m);
            for(int nn = 0; nn < intervalos; nn++){
              xn = xiPasso(a, h, nn);
              for(int o = 0; o < intervalos; o++){
                xo = xiPasso(a, h, o);
                for(int p = 0; p < intervalos; p++)
                  sum += fxStyblinskiTang((double[]){ xi, xj, xk, xl, xm, xn, xo, xiPasso(a, h, p) }, n);
              }
            }
          }
        }
      }
    }
  }
  return (pow(h, n) * sum);
}

double calcIntegralTrapezioCompostaStyblinskiTang(double a, double b, int intervalos, int n){
  if(n==1) return calcIntegralTrapezioCompostaStyblinskiTang1(a, b, intervalos, n);
  if(n==2) return calcIntegralTrapezioCompostaStyblinskiTang2(a, b, intervalos, n);
  if(n==3) return calcIntegralTrapezioCompostaStyblinskiTang3(a, b, intervalos, n);
  if(n==4) return calcIntegralTrapezioCompostaStyblinskiTang4(a, b, intervalos, n);
  if(n==5) return calcIntegralTrapezioCompostaStyblinskiTang5(a, b, intervalos, n);
  if(n==6) return calcIntegralTrapezioCompostaStyblinskiTang6(a, b, intervalos, n);
  if(n==7) return calcIntegralTrapezioCompostaStyblinskiTang7(a, b, intervalos, n);
  return calcIntegralTrapezioCompostaStyblinskiTang8(a, b, intervalos, n);
}

/**
* Integral Montecarlo multidimensional - f(x) tal que x é um vetor com cardinaliade n
**/
double calcIntegralMontecarloStyblinskiTang(double a, double b, int intervalos, int n){
  double sum = 0;
  double *x = constroiVetor(n); 
  for(int i = 0; i < intervalos; ++i){
    for(int count = 0; count < n; count++) x[count] = a + (( double ) rand () / RAND_MAX) * (b - a);
    sum += fxStyblinskiTang(x, n);
  }
  return (sum / intervalos) * pow((b - a), n);
}

int main(int argc, char **argv){
  if(argc < 4){
    printf("Exemplo de execução para ordem = 2, Integral definida em a = 1 e b = 3:\n");
    printf("\tEX: ./a.out 2 1 3\n");
    return 0;
  }
  srand(1234);
  int n = atoi(argv[1]); double a = atof(argv[2]); double b = atof(argv[3]);
  printf(
    "IntegralTrapComposta(a = %lf, b = %lf)f(x).dx = %lf\n",
    a,
    b,
    calcIntegralTrapezioCompostaStyblinskiTang(a, b, AMOSTR, n)
  );
  printf(
    "IntegralMontecarlo(a = %lf, b = %lf)f(x).dx = %lf\n",
    a,
    b,
    calcIntegralMontecarloStyblinskiTang(a, b, AMOSTR, n)
  );
}

// typedef struct p{
//   int ordem;
//   double *coef;
// }polinomio;

// typedef struct cp{
//   double *x;
//   double *y;
//   int n;
// }conjuntoPontos;

// double *constroiVetor(int n){ return (double*)malloc(sizeof(double) * n); }

// conjuntoPontos *constroiConjuntoPontos(double x[], double y[], int n){
//   conjuntoPontos *c = (conjuntoPontos *)malloc(sizeof(conjuntoPontos));
//   c->n = n;
//   c->x = (double *)malloc(sizeof(double) * n);
//   c->y = (double *)malloc(sizeof(double) * n);
//   for(int i = 0; i < n; i++){ c->x[i] = x[i]; c->y[i] = y[i]; }
//   return c;
// }

// polinomio *constroiPolinomio(int n, double coef[]){
//   polinomio *p = (polinomio *)malloc(sizeof(polinomio));
//   p->ordem = n;
//   p->coef = (double *)malloc(sizeof(double) * (p->ordem + 1));
//   for(int i = 0; i < (p->ordem + 1); i++) p->coef[i] = coef[i];
//   return p;
// }

// polinomio *lagrange(){ return NULL; }

// void mostraPolinomio(polinomio *p){
//   printf("f(x) = ");
//   for(int i = 0; i < (p->ordem + 1); i++){
//     if(p->coef[i]<0.0) printf("%lf*x^%d ", p->coef[i], (p->ordem - i));
//     else printf("+%lf*x^%d ", p->coef[i],(p->ordem - i));
//   }
//   printf("\n\n");
// }

/**
* Integral Montecarlo unidimensional - f(x) tal que x tem cardinalidade 1
**/
// double calcIntegralMontecarlo(polinomio *p, double a, double b, int n){
//   double x, sum = 0;
//   for(int i = 0; i < n; ++i) sum += fx(p, a + (( double ) rand () / RAND_MAX) * (b - a));
//   return (sum / n) * (b - a);
// }

// void mostraConjuntoPontos(conjuntoPontos *c){
//   printf("(x,y) = {");
//   for(int i = 0; i < ( c->n - 1 ); i++) printf(" (%lf, %lf),", c->x[i], c->y[i]);
//   printf(" (%lf, %lf) }\n\n", c->x[c->n - 1], c->y[c->n - 1]);
// }

// double fx(polinomio *p, double x){
//   double res = 0;
//   for(int i = 0; i < (p->ordem + 1); i++)
//     res += p->coef[i] * pow(x,(p->ordem - i));
//   return res;
// }


// int main(){
//   int ordem = 1;
//   double coefs[] = (double[]) { 1, 1 };
//   polinomio *p = constroiPolinomio(ordem, coefs);
//   mostraPolinomio(p);
  
//   double a = 1; double b = 2;
//   printf("IntegralTrapComposta(a = %lf, b = %lf)f(x).dx = %lf\n", a, b, calcIntegralTrapezioComposta(p, a, b, AMOSTR));
//   printf("IntegralMontecarlo(a = %lf, b = %lf)f(x).dx = %lf\n", a, b, calcIntegralMontecarlo(p, a, b, AMOSTR));
// }

// int main(){
//   int n = 3;
//   double x[] = (double[]) {-1, 0, 2 };
//   double y[] = (double[]) { 4, 1,-1 };
//   conjuntoPontos *cp = constroiConjuntoPontos(x, y, n);
//   mostraConjuntoPontos(cp);
// }
