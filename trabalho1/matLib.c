/**
* @file matLib.c
* @brief Biblioteca para utilidades de Matriz, Array e Sistemas Lineares
*/
#include <stdio.h>
#include <math.h>
#include <float.h>
#include <stdlib.h>
#include "utils.h"
#include "matLib.h"

/**
* Função que aloca memoria para um vetor
*/
double *constroiVetor(int n){ return (double *)malloc(sizeof(double) * n); }

/**
* Função que aloca memoria para um vetor com valores 0
*/
double *constroiVetorZero(int n){ return (double *)calloc(sizeof(double), n); }

/**
* Função que aloca memoria para uma matriz com l linhas e c colunas
*/
double **constroiMat(int l, int c){
  double **a = (double **) malloc(sizeof(double *) * l);
  for(int i = 0; i < l; i++) a[i] = constroiVetor(c);
  return a;
}

/**
* Função que retorna uma matriz identidade
*/
double **constroiMatIdent(int l, int c){
  double **a = (double **) malloc(sizeof(double *) * l);
  for(int i = 0; i < l; i++) a[i] = constroiVetor(c);
  for(int i = 0; i < l; i++) for(int j = 0; j < c; j++) a[i][j] = ((i==j) ? 1 : 0);
  return a; 
}

/**
* Função que destroi uma matriz
*/
void destroiMat(double **M, int l, int c){
  for(int i = 0; i < c; i++) free(M[i]);
  free(M);
}

/**
* Função que mostra o conteudo de um array
*/
void mostraArray(double *array, int n){
  for(int i = 0; i < n; i++) printf("%lf ", array[i]);
  printf("\n"); 
}

/**
* Função que mostra o conteudo de uma matriz
*/
void mostraMat(double **A, int l, int c){
  if(A == NULL) { printf("Matriz nao existe\n"); return; }
  printf("\n l = %d, c = %d\n", l, c);
  for(int i = 0; i < l; i++) mostraArray(A[i], c); 
}

/**
* Função que copia o array A para o Array B
*/
void copiaArray(double *A, double *B, int n){ for(int i = 0; i < n; i++) B[i] = A[i]; }

/**
* Função que copia uma Matriz A para uma Matriz B quadrada em espaco R(n x n)
*/
void copiaMatriz(double **A, double **B, int n){
  for(int i = 0; i < n; i++)
    for(int j = 0; j < n; j++) B[i][j] = A[i][j];
}

/**
* Função que retorna um array com a soma de B e C
*/
double *somaArray(double *B, double *C, int N){
  double *r = constroiVetorZero(N);
  for(int i = 0; i < N; i++) r[i] = B[i] + C[i];
  return r;
}

/**
* Função que retorna um array com a subtracao de B e C
*/
double *subtraiArray(double *B, double *C, int N){
  double *r = constroiVetorZero(N);
  for(int i = 0; i < N; i++) r[i] = B[i] - C[i];
  return r;
}

/**
* Função retorna um array com a multiplicacao escalar alpha*B
*/
double *multEscalar(double alpha, double *b, int N){
  double *r = constroiVetorZero(N);
  for(int i = 0; i < N; i++) r[i] = (alpha * b[i]);
  return r;
}

/**
* Função retorna a multiplicacao escalar de dois arrays
*/
double multEscalarArray(double *a, double *b, int N){
  double r = 0.0;
  for(int i = 0; i < N; i++) r += (a[i] * b[i]);
  return r;
}

/**
* Função retorna a norma euclidiana de A
*/
double normaEuclidiana(double *A, int N){
  double norma = 0;
  for(int i = 0; i < N; i++) norma += (A[i]*A[i]);
  return sqrt(norma);
}

/**
* Função retorna a norma maxima de A
*/
double normaMax(double *A, int N){
  double norma = 0;
  for(int i = 0; i < N; i++) if(fabs(A[i])>norma) norma = fabs(A[i]);
  return norma;
}

/**
* Função que verifica se A é diagonal Dominante com o criterio de linhas
*/
int diagonalDominante(double **A, int N){
  double sum ;
  for(int i = 0; i < N; i++){
    sum = 0;
    for(int j = 0; j < N; j++) if (i!=j) sum += fabs(A[i][j]);
    if(sum > fabs(A[i][i])) return 0;
  }
  return 1;
}

/**
* Função que retorna uma Matriz triangular inferior de M
*/
double **triangularL(double **M, int N){
  double **R = constroiMatIdent(N, N);
  for(int i = 0; i < N; i++)
    for(int j = 0; j < N; j++)
      R[i][j] = ((j >= i) ? 0 : M[i][j] );
  return R;
}

/**
* Função que retorna uma Matriz triangular superior de M
*/
double **triangularU(double **M, int N){
  double **R = constroiMatIdent(N, N);
  for(int i = 0; i < N; i++)
    for(int j = 0; j < N; j++)
      R[i][j] = ((j <= i) ? 0 : M[i][j] );
  return R;
}

/**
* Função que retorna uma Matriz diagonal de M
*/
double **diagonalD(double **M, int N){
  double **R = constroiMatIdent(N, N);
  for(int i = 0; i < N; i++) R[i][i] = M[i][i];
  return R;
}

/**
* Função que retorna a eliminacao de Gauss contendo a matriz Superior e armazenando as operacoes matriciais em L
*/
double **eliminacaoGauss(double **A, int n, double **L){
  double m, pivot;
  double **NA = constroiMat(n,n);
  copiaMatriz(A, NA, n);
  /* Aplicando pivoteamento p/ as (n-1) primeiras colunas abaixo da diagonal j */
  for(int j = 0; j < (n - 1); j++){
    /* Aplicando eliminacao p/ as linhas i abaixo do pivot da diagonal j */
    for(int i = (j + 1); i < n; i++){
      pivot = NA[j][j];
      if(pivot == 0.0)
      {
        for(int jj = j + 1; jj < n; jj++){
          if(NA[jj][j] != 0)
          {
            pivot = NA[jj][j];
            double *aux = constroiVetor(n);
            for(int c = 0; c < n; c++){
              aux[c] = NA[jj][c];
              NA[jj][c] = NA[j][c];
              NA[j][c] = aux[c];
              aux[c] = L[jj][c];
              L[jj][c] = L[j][c];
              L[j][c] = aux[c];
            }
            break;
          }
        }
      }
      m = (NA[i][j] / pivot);
      for(int k = 0; k < n; k++) NA[i][k] -= (m * NA[j][k]);
      for(int k = 0; k < n; k++) L[i][k] -= (m * L[j][k]);
    }
  }
  return NA;
}

/**
* Função que retorna a eliminacao de Gauss contendo a matriz Inferior e armazenando as operacoes matriciais em L
*/
double **eliminacaoGaussInversa(double **A, int n, double **L){
  double m, pivot;
  double **NA = constroiMat(n,n);
  copiaMatriz(A, NA, n);
  /* Aplicando pivoteamento p/ as (n-1) primeiras colunas abaixo da diagonal j */
  for(int j = (n-1); j > 0; j--){
    /* Aplicando eliminacao p/ as linhas i acima do pivot da diagonal j */
    for(int i = (j - 1); i >= 0; i--){
      pivot = NA[j][j];
      if(pivot == 0.0)
      {
        for(int jj = j + 1; jj < n; jj++){
          if(NA[jj][j] != 0)
          {
            pivot = NA[jj][j];
            double *aux = constroiVetor(n);
            for(int c = 0; c < n; c++){
              aux[c] = NA[jj][c];
              NA[jj][c] = NA[j][c];
              NA[j][c] = aux[c];
              aux[c] = L[jj][c];
              L[jj][c] = L[j][c];
              L[j][c] = aux[c];
            }
            break;
          }
        }
      }
      m = (NA[i][j] / pivot);
      for(int k = 0; k < n; k++) NA[i][k] -= (m * NA[j][k]);
      for(int k = 0; k < n; k++) L[i][k] -= (m * L[j][k]);
    }
  }
  return NA;
}

/**
* Função que retorna a eliminacao de colunas e armazenando as operacoes matriciais em L
*/
double **eliminacaoColunas(double **A, int n, double **L){
  double m, pivot;
  double **NA = constroiMat(n,n);
  copiaMatriz(A, NA, n);
  for(int i=0;i<n;i++){
    pivot = NA[i][i];
    if(pivot != 0.0){
      m = 1 / pivot;
      NA[i][i] = (m * NA[i][i]);
      for(int k = 0; k < n; k++) L[i][k] = (m * L[i][k]);
    }
  }
  return NA;
}

/**
* Fatoracao LU
*/
void lu(double **A, int n, double **L, double **U){
  double m, pivot;
  for(int i=0;i<n;i++) for(int j=0;j<n;j++) U[i][j] = A[i][j];
  for(int j = 0; j < (n - 1); j++){
    for(int i = (j + 1); i < n; i++){
      pivot = U[j][j];
      m = (U[i][j] / pivot);
      L[i][j] = m;
      for(int k = 0; k < n; k++) U[i][k] -= (m * U[j][k]);
    }
  }
}

/**
* Fatoracao LDU
*/
void ldu(double **A, int n, double **L, double **D, double **U){
  lu(A, n, L, U);
  for(int i=0; i < n; i++)
  {
    if(U[i][i] != 1 && U[i][i] != 0){
      D[i][i] = U[i][i];
      for(int j=0; j < n; j++) U[i][j] = (U[i][j] / U[i][i]);
    }
  }
}

/**
* Função que calcula o determinande atraves dos determinantes de cofatores
*/
double det(double **M, int N){
  if(N==1) return M[0][0];
  if(N==2) return (M[0][0] * M[1][1]) - (M[0][1] * M[1][0]);
  double d = 0;
  for(int j = 0; j < N; j++)
    d += (j%2==0) ? (M[0][j] * detCof(M, N, 0, j)) : ( -1 * M[0][j] * detCof(M, N, 0, j));
  return d;
}

/**
* Função que calcula o determinante do cofator para o elemento cofI e cofJ
*/
double detCof(double **M, int N, int cofI, int cofJ){
  int newN = N - 1;
  int iC, jC; iC = 0;
  double **C = constroiMat(newN, newN);
  for(int i = 0; i < N; i++){
    jC = 0;
    if(i != cofI){
      for(int j = 0; j < N; j++)
        if (j != cofJ) C[iC][jC++] = M[i][j];
      iC++;
    }
  }
  double d = det(C, newN);
  destroiMat(C, newN, newN);
  return d;
}

/**
* Função que calcula o criterio de Linhas
*/
int criterioLinhas(double **A, int l, int c){
  double lineSum;
  for(int i=0; i < l; i++){
    lineSum = 0;
    for(int j=0; j < c; j++) if (j!=i) lineSum += fabs(A[i][j]);
    if(fabs(A[i][i]) <= lineSum) return 0;
  }
  return 1;
}

/**
* Função que calcula o criterio de Sassenfeld
*/
int criterioSassenfeld(double **A, int l, int c){
  double *B = constroiVetorZero(l);

  for(int i = 0; i < l; i++){
    for(int j = 0; j < (i - 1); j++)
      B[i] += B[j]*fabs(A[i][j]);
    for(int j = (i + 1); j < c; j++)
      B[i] += fabs(A[i][j]);
    B[i] = (B[i] / fabs(A[i][i]) );
  }  
  
  return (normaMax(B, l) < 1.0);
}

/**
* Função que verifica a equivalencia de matrizes
*/
int equivalencia(double **A, double **B, int l, int c){
  for(int i = 0; i < l; i++)
    for(int j = 0; j < c; j++) if(A[i][j]!=A[i][j]) return 0;
  return 1;
}

/**
* Função que verifica se A é simétrica
*/
int simetrica(double **A,int l, int c){
  for(int i = 0; i < l; i++)
    for(int j = 0; j < c; j++) if(A[i][j]!=A[j][i]) return 0;
  return 1;
}

/**
* Função que retorna transposta de M
*/
double **transposta(double **M, int N){
  double **C = constroiMat(N, N);
  for(int i = 0; i < N; i++)
    for(int j = 0; j < N; j++) 
      C[i][j] = M[j][i];
  return C; 
}

/**
* Função que retorna a matriz de cofatores de M
*/
double **cof(double **M, int N){
  double **C = constroiMat(N, N);
  if(N==1){ C[0][0] = M[0][0]; return C; }
  for(int i = 0; i < N; i++)
    for(int j = 0; j < N; j++) 
      C[i][j] = ((i + j)%2==0) ? detCof(M, N, i, j) : -1*detCof(M, N, i, j);
  return C;
}

/**
* Função que retorna a matriz de Adjunta de M baseada na transposta dos cofatores
*/
double **adj(double **M, int N){ return transposta(cof(M,N), N); }

/**
* Função que calcula a Matriz inversa de M a partir da Adjunta (Obs: Nao utilizar devido ao alto consumo de memoria)
*/
double **inversa(double **M, int N){
  double d = det(M, N);
  if(d==0) return NULL;
  double **MInv = constroiMat(N, N);
  if(N==1) { MInv[0][0] = 1/M[0][0]; return MInv; }
  double **adjM = adj(M,N);
  for(int i = 0; i < N; i++)
    for(int j = 0; j < N; j++) MInv[i][j] = (1 / d) * adjM[i][j];
  return MInv;
}

/**
* Função que calcula a Matriz inversa de M a partir das eliminacoes de Gauss e aplica as operacoes na matriz identidade
*/
double **inversaEliminacao(double **A, int n){
  double **L = constroiMatIdent(n, n);
  double **U = eliminacaoGauss(A,n,L);
  double **D = eliminacaoGaussInversa(U,n,L);
  eliminacaoColunas(D,n,L);
  return L;
}

/**
* Função que retorna a soma das matrizes A e B
*/
double **somaMatriz(double **A, double **B, int N){
  double **C = constroiMat(N, N);
  for(int i = 0; i < N; i++)
    for(int j = 0; j < N; j++)
      C[i][j] = A[i][j] + B[i][j];
  return C;
}

/**
* Função que retorna a multiplicao de um escalar por uma matriz
*/
double **multiplicaMatrizEscalar(double **A, double w, int N){
  double **C = constroiMat(N, N);
  for(int i = 0; i < N; i++)
    for(int j = 0; j < N; j++)
      C[i][j] = A[i][j] * w;
  return C;
}

/**
* Função que retorna a multiplicao de uma Matriz A e B quadradas
*/
double **multiplicaMatriz(double **A, double **B, int N){
  double **C = constroiMat(N, N);
  for(int i = 0; i < N; i++)
    for(int j = 0; j < N; j++)
      for(int k = 0; k < N; k++) C[i][j] += (A[i][k] * B[k][j]);
  return C;
}
