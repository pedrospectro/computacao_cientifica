#include <stdio.h>
#include <stdlib.h>

#define MAX 10

void gaussSiedelOD(double x0, double y0, double xn, double yn, int n, double *Y){
  int i, j;
  double s, xi, dx;
  // define passo
  dx = (xn - x0) / n;

  // coloca condições de contorno em Y
  Y[0] = y0; Y[n] = yn;
  
  // Gauss-Seidel
  for (j = 0; j < MAX; ++j) {
    xi = x0;
    for (i = 1; i < n; ++i) {
      xi += dx;
      s = (1 - xi * dx / 2) * Y[i - 1] + (1 + xi * dx / 2) * Y[i + 1];
      Y[i] = -(dx * dx * xi - s) / (2 + dx * dx * xi * xi);
    }
  }
}

void mostraVetor(double *Y, int tam){
  for(int i = 0; i < tam; i++) printf("%lf ", Y[i]);
  printf("\n");
}

int main()
{
  int n = 5;
  double x0 = 1;
  double y0 = 3;
  double xn = 11;
  double yn = 2.5;
  double *Y = (double *)calloc((n + 1), sizeof(double));
  mostraVetor(Y, n + 1);
  gaussSiedelOD(x0, y0, xn, yn, n, Y);
  mostraVetor(Y, n + 1);
  return 0;
}