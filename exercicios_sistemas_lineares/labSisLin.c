#include <stdio.h>
#include <math.h>
#include "utils.h"
#include "SistemasLineares.h"

void metodoMatriz()
{
  int n;
  double  **A, *B, *X, tIteracao, tTotal, relativeError;
  while(scanf("%d", &n)!=EOF){
    A = constroiMat(n, n);
    B = constroiVetor(n);
    /* Leitura de dados */
    for(int i = 0; i < n; i++)
      for(int j = 0; j < n; j++) scanf("%lf", &A[i][j]);
    for(int i = 0; i < n; i++) scanf("%lf", &B[i]);
    /* Aplicacao dos metodos de sistemas lineares */
    X = constroiVetorZero(n);

    relativeError = gaussSeidel(A, B, X, n, &tIteracao, &tTotal);
    printf("\nMetodo de Gauss:\n");
    mostraArray(X, n);
    printf("Erro Relativo Max = %lf\n", relativeError);
    printf("tIteracao = %lf , tTotal = %lf\n", tIteracao, tTotal);
    X = constroiVetorZero(n);

    relativeError = jacobi(A, B, X, n, &tIteracao, &tTotal);
    printf("\nMetodo de Jacobi:\n");
    mostraArray(X, n);
    printf("Erro Relativo Max = %lf\n", relativeError);
    printf("tIteracao = %lf , tTotal = %lf\n", tIteracao, tTotal);
  }
}

void metodoArray()
{
  int n;
  double *A, *B, *X, tIteracao, tTotal, relativeError;
  while(scanf("%d", &n)!=EOF){
    A = constroiMatArray(n, n);
    B = constroiVetor(n);
    /* Leitura de dados */
    for(int i = 0; i < n; i++)
      for(int j = 0; j < n; j++) scanf("%lf", &A[(i * n) + j]);
    for(int i = 0; i < n; i++) scanf("%lf", &B[i]);
    /* Aplicacao dos metodos de sistemas lineares */
    X = constroiVetorZero(n);

    relativeError = gaussSeidelArray(A, B, X, n, &tIteracao, &tTotal);
    printf("\nMetodo de Gauss:\n");
    mostraArray(X, n);
    printf("Erro Relativo Max = %lf\n", relativeError);
    printf("tIteracao = %lf , tTotal = %lf\n", tIteracao, tTotal);
    X = constroiVetorZero(n);

    relativeError = jacobiArray(A, B, X, n, &tIteracao, &tTotal);
    printf("\nMetodo de Jacobi:\n");
    mostraArray(X, n);
    printf("Erro Relativo Max = %lf\n", relativeError);
    printf("tIteracao = %lf , tTotal = %lf\n", tIteracao, tTotal);
  }
}

void resolveMatriz(){
  int n;
  double  **A, *B, *X;
  while(scanf("%d", &n)!=EOF){
    A = constroiMat(n, n);
    B = constroiVetor(n);
    /* Leitura de dados */
    for(int i = 0; i < n; i++)
      for(int j = 0; j < n; j++) scanf("%lf", &A[i][j]);
    for(int i = 0; i < n; i++) scanf("%lf", &B[i]);

    X = retrosubstituicao(A, B ,n);
    for(int i = 0; i < n; i++){
      double res = 0;
      for(int j = 0; j < n; j++) res += A[i][j]*X[j];
    }
    mostraArray(X, n); 
  }
}

void resolveArray(){
  int n;
  double *A, *B, *X;
  while(scanf("%d", &n)!=EOF){
    A = constroiMatArray(n, n);
    B = constroiVetor(n);
    /* Leitura de dados */
     for(int i = 0; i < n; i++)
       for(int j = 0; j < n; j++) scanf("%lf", &A[(i * n) + j]);
    for(int i = 0; i < n; i++) scanf("%lf", &B[i]);

    X = retrosubstituicaoArray(A, B ,n);
    for(int i = 0; i < n; i++){
      double res = 0;
      for(int j = 0; j < n; j++) res += A[(i*n)+j]*X[j];
    }
    mostraArray(X, n); 
  }
}

int main(){
  //resolveArray();
  //resolveMatriz();
  //metodoArray();
  metodoMatriz();
}
