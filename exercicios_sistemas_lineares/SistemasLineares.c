#include <stdio.h>
#include <math.h>
#include "utils.h"
#include "SistemasLineares.h"

/* Metodos Construtores */
double *constroiVetor(int n){ return (double *)malloc(sizeof(double) * n); }

double *constroiVetorZero(int n){ return (double *)calloc(sizeof(double), n); }

double *constroiMatArray(int l, int c){ return (double *) malloc(sizeof(double) * l * c); }

double **constroiMat(int l, int c){
  double **a = (double **) malloc(sizeof(double *) * l);
  for(int i = 0; i < l; i++) a[i] = constroiVetor(c);
  return a;
}

/* Metodos Auxiliares */
void mostraArray(double *array, int n){
  for(int i = 0; i < n; i++) printf("%lf ", array[i]);
  printf("\n"); 
}

void mostraMat(double **A, int l, int c){
  printf("\n l = %d, c = %d\n", l, c);
  for(int i = 0; i < l; i++) mostraArray(A[i], c); 
}

void mostraMatArray(double *A, int l, int c){
  printf("\n l = %d, c = %d\n", l, c);
  for(int i = 0; i < l; i++){
    for(int j = 0; j < c; j++) printf("%lf ", A[(i * c) + j]);
    printf("\n"); 
  }
}

void copiaArray(double *A, double *B, int n){ for(int i = 0; i < n; i++) B[i] = A[i]; }

/* Eliminacoes de Gauss p matriz aumentada A|B onde A é uma matriz */
void eliminacaoGauss(double **A, double *B, int n){
  double m, pivot;
  /* Aplicando pivoteamento p/ as (n-1) primeiras colunas abaixo da diagonal j */
  for(int j = 0; j < (n - 1); j++){
    /* Aplicando eliminacao p/ as linhas i abaixo do pivot da diagonal j */
    for(int i = (j + 1); i < n; i++){
      pivot = A[j][j];
      if(pivot == 0.0)
      {
        for(int jj = j + 1; jj < n; jj++){
          if(A[jj][j] != 0)
          {
            pivot = A[jj][j];
            double *aux = constroiVetor(n);
            for(int c = 0; c < n; c++){
              aux[c] = A[jj][c];
              A[jj][c] = A[j][c];
              A[j][c] = aux[c];
            }
            double f = B[j];
            B[j] = B[jj];
            B[jj] = f;
            break;
          }
        }
      }
      m = (A[i][j] / pivot);
      /* Aplicando Linhai = Linhai - (m * Linhaj) e  Bi = Bi - (m * Bj)*/
      for(int k = 0; k < n; k++) A[i][k] -= (m * A[j][k]);
      B[i] -= (m * B[j]);
    }
  }
}

/* Eliminacoes de Gauss p matriz aumentada A|B onde A é um vetor que representa uma matriz */
void eliminacaoGaussArray(double *A, double *B, int n)
{
  double m, pivot;
  for(int i = 0; i < (n - 1); i++){
    for(int k = ((i + 1) * n); k < (n * n); k = (k + n)){
      pivot = A[(i * n) + i];
      if(pivot == 0.0)
      {
        for(int jj = i + 1; jj < n; jj++){
          if(A[(jj * n) + i] != 0)
          {
            pivot = A[(jj * n) + i];
            double *aux = constroiVetor(n);
            for(int c = 0; c < n; c++){
              aux[c] = A[(jj * n) + c];
              A[(jj * n) + c] = A[(i * n) + c];
              A[(i * n) + c] = aux[c];
            }
            double f = B[i];
            B[i] = B[jj];
            B[jj] = f;
            break;
          }
        }
      }
      m = (A[k + i] / pivot);
      for(int j = 0; j < n; j++) A[k + j] -= (m * A[(i * n) + j]);
      B[k / n] -= (m * B[((i * n) + i) / n]);
    }
  }
}

/* Retrosubstituicao */
double *retrosubstituicao(double  **A, double *B, int n){
  double *X = constroiVetor(n);
  eliminacaoGauss(A, B, n);

  X[n-1] = B[n-1]/A[n-1][n-1];
  for(int i = (n - 2); i >= 0; i--)
  {
    double restValue = 0.0;
    for(int j = 0; j < n; j++) if(j != i) restValue += (A[i][j] * X[j]);
    X[i] = (B[i] - restValue) / A[i][i];
  } 

  return X;
}

double *retrosubstituicaoArray(double  *A, double *B, int n){
  double *X = constroiVetor(n);

  eliminacaoGaussArray(A, B, n);

  X[n-1] = B[n-1]/A[((n-1)*n) + (n-1)];
  for(int i = (n - 2); i >= 0; i--)
  {
    double restValue = 0.0;
    for(int j = 0; j < n; j++) if(j != i) restValue += (A[(i*n)+j] * X[j]);
    X[i] = (B[i] - restValue) / A[(i*n)+i];
  }

  return X;
}

/* Calculo do erro */
void calcErro(double *X, double *XNew, double *erroRelativo, int n){
  double erroRelativoMax = -1;
  for(int i = 0; i < n; i++) {
    *erroRelativo = fabs((XNew[i] - X[i]));
    if(*erroRelativo >= erroRelativoMax) erroRelativoMax = *erroRelativo;
  }
  *erroRelativo = erroRelativoMax; 
}

/* Metodo Jacobi */
double jacobi(double  **A, double *B,  double *X,  int n, double *tIteracao, double *tTotal)
{
  int it = 0;
  double *XNew = constroiVetorZero(n);
  double erroRelativo, t0, tn;
  t0 = timestamp();
  do{
    it++;
    for(int i = 0; i < n; i++)
    {
      double restValue = 0.0;
      for(int j = 0; j < n; j++) if(j != i) restValue += (A[i][j] * X[j]);
      XNew[i] = (B[i] - restValue) / A[i][i];
    }
    calcErro(X, XNew, &erroRelativo, n);
    copiaArray(XNew, X, n);
  } while((erroRelativo > EPS) && (it < MAXIT));
  tn = timestamp();
  *tIteracao = (tn - t0);
  *tTotal = *tIteracao;
  return erroRelativo;
}

double jacobiArray(double  *A, double *B,  double *X,  int n, double *tIteracao, double *tTotal)
{
  int it = 0;
  double *XNew = constroiVetorZero(n);
  double erroRelativo, t0, tn;
  t0 = timestamp();
  do{
    it++;
    for(int i = 0; i < n; i++)
    {
      double restValue = 0.0;
      for(int j = 0; j < n; j++) if(j != i) restValue += (A[(i * n) + j] * X[j]);
      XNew[i] = (B[i] - restValue) / A[(i * n) + i];
    }
    calcErro(X, XNew, &erroRelativo, n);
    copiaArray(XNew, X, n);
  } while((erroRelativo > EPS) && (it < MAXIT));
  tn = timestamp();
  *tIteracao = (tn - t0);
  *tTotal = *tIteracao;
  return erroRelativo;
}

/* Metodo Gauss siedel */
double gaussSeidel(double  **A, double *B,  double *X,  int n, double *tIteracao, double *tTotal)
{
  int it = 0;
  double *XNew = constroiVetorZero(n);
  double erroRelativo, t0, tn;
  t0 = timestamp();
  do{
    it++;
    for(int i = 0; i < n; i++)
    {
      double restValue = 0.0;
      for(int j = 0; j < n; j++) 
        if(j != i)
          (j>i) ? (restValue += (A[i][j] * X[j])) : (restValue += (A[i][j] * XNew[j]));
      XNew[i] = (B[i] - restValue) / A[i][i];
    }
    calcErro(X, XNew, &erroRelativo, n);
    copiaArray(XNew, X, n);
  } while((erroRelativo > EPS) && (it < MAXIT));
  tn = timestamp();
  *tIteracao = (tn - t0);
  *tTotal = *tIteracao;
  return erroRelativo;
}

double gaussSeidelArray(double  *A, double *B,  double *X,  int n, double *tIteracao, double *tTotal)
{
  int it = 0;
  double *XNew = constroiVetorZero(n);
  double erroRelativo, t0, tn;
  t0 = timestamp();
  do{
    it++;
    for(int i = 0; i < n; i++)
    {
      double restValue = 0.0;
      for(int j = 0; j < n; j++) 
        if(j != i)
          (j>i) ? (restValue += (A[(i * n) + j] * X[j])) : (restValue += (A[(i * n) + j] * XNew[j]));
      XNew[i] = (B[i] - restValue) / A[(i * n) + i];
    }
    calcErro(X, XNew, &erroRelativo, n);
    copiaArray(XNew, X, n);
  } while((erroRelativo > EPS) && (it < MAXIT));
  tn = timestamp();
  *tIteracao = (tn - t0);
  *tTotal = *tIteracao;
  return erroRelativo;
}
