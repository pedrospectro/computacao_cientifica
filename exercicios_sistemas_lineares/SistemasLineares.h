#ifndef __SISLINEAR_H__
#define __SISLINEAR_H__

// ParÃ¢metros para teste de convergÃªncia
#define MAXIT 500
#define EPS 1.0e-4

/* Construtores de Arrays e Matrizes */
double *constroiVetor(int n);
double *constroiVetorZero(int n);
double **constroiMat(int l, int c);
double *constroiMatArray(int l, int c);
void copiaArray(double *A, double *B, int n);

/* Metodos auxiliares */
void mostraArray(double *B, int n);
void mostraMat(double  **A, int l, int c);
void mostraMatArray(double  *A, int l, int c);

/* Eliminacoes de Gauss p/ sistema Ax = b */
void eliminacaoGauss(double **A, double *B, int n);
void eliminacaoGaussArray(double  *A, double *B, int n);

/* Retrosubstituicao */
double *retrosubstituicao(double  **A, double *B, int n);
double *retrosubstituicaoArray(double  *A, double *B, int n);

/* Calculo do erro */
void calcErro(double *X, double *XNew, double *erroRelativo, int n);

// Metodo de JACOBI p/ sistema AX = b
double jacobi(double **A, double *B,  double *X,  int n, double *tIteracao, double *tTotal);
double jacobiArray(double *A, double *B,  double *X,  int n, double *tIteracao, double *tTotal);
// Metodo GAUSS_SEIDEL p/ sistema AX = b
double gaussSeidel(double **A, double *B,  double *X,  int n, double *tIteracao, double *tTotal);
double gaussSeidelArray(double *A, double *B,  double *X,  int n, double *tIteracao, double *tTotal);

#endif // __SISLINEAR_H__