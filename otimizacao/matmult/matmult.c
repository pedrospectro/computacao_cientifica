#include <stdio.h>
#include <stdlib.h>    /* exit, malloc, calloc, etc. */
#include <string.h>
#include <getopt.h>    /* getopt */

#include "matmult.h"
#include "matriz.h"
#include "utils.h"

/**
 * Exibe mensagem de erro indicando forma de uso do programa e termina
 * o programa.
 */

static void usage(char *progname)
{
  fprintf(stderr, "Forma de uso: %s [ -n <ordem> ] [ -b <blocking> ] \n", progname);
  exit(1);
}



/**
 * Programa principal
 * Forma de uso: matmult [ -n <ordem> ] [ -m <stride> ] [ -b <blocking> ]
 * -n <ordem>: ordem das matrizes quadradas e de vetores
 * -b <blocking>: fator de 'blocking'
 *
 */

int main (int argc, char *argv[]) 
{
  int c, b=BLK, n=N;
  
  MatRow A, AA, AAA, B, C, CC, CCC;
  Vetor vet, res;

  double t, t_uj, t_blk;
  
  /* =============== TRATAMENTO DE LINHA DE COMAANDO =============== */

  char *opts = "n:b:";
  c = getopt (argc, argv, opts);
  
  while ( c != -1 ) {
    switch (c) {
    case 'n':  n = atoi(optarg);   break;
    case 'b':  b = atoi(optarg);   break;
    default:   usage(argv[0]);
    }
      
    c = getopt (argc, argv, opts);
  }

  /* ================ FIM DO TRATAMENTO DE LINHA DE COMANDO ========= */
  
  
  srand(20182);
  
  A = geraMat (n, n);
  AA = geraMat (n, n);
  AAA = geraMat (n, n);
  B = geraMat (n, n);
  C = geraMat (n, n);
  CC = geraMat (n, n);
  CCC = geraMat (n, n);
  vet = geraVetor (n);
    
#ifdef DEBUG
  prnMat (A, n);
  prnMat (B, n);
  prnVetor (vet, n);
  printf ("=================================\n\n");
#endif /* DEBUG */
  
  double t0;

  // t0 = timestamp();
  // mt (A, B, n);
  // printf("Transposta = %lf\n", timestamp() - t0);

  // //prnMat (A, n); 

  // t0 = timestamp();
  // mt_uj (AA, B, n);
  // printf("Transposta uj = %lf\n", timestamp() - t0);

  // //prnMat (AA, n); 

  // t0 = timestamp();
  mt_blk (A, B, n, b);
  // printf("Transposta ujb uj = %lf\n", timestamp() - t0);

  //prnMat (AAA, n); 

  zeraMat (C, n, n);
  t0 = timestamp();
  mmm (A, B, n, C);
  printf("Multiplicacao = %lf\n", timestamp() - t0);

  zeraMat (CC, n, n);
  t0 = timestamp();
  mmm_uj (A, B, n, CC);
  printf("Multiplicacao uj = %lf\n", timestamp() - t0);

  zeraMat (CCC, n, n);
  t0 = timestamp();
  mmm_blk (A, B, n, b, CCC);
  printf("Multiplicacao ujb = %lf\n", timestamp() - t0);

  printf("equivalentes = %d\n", equiv(C, CC, n)); 
  printf("equivalentes = %d\n", equiv(C, CCC, n)); 

//#ifdef DEBUG
  //prnMat (C, n);
  //prnVetor (res, n);
//#endif /* DEBUG */

  free (A);
  free (B);
  free (C);
  free (vet);
}

