/**
* @file cgSolver.c
* @brief Programa principal para o calculo de uma aproximacao de um sitema AX=B  utilizando
* Gradiente Conjugado com matriz de banda simetrica
*/
#include <stdio.h>
#include <math.h>
#include <float.h>
#include "utils.h"
#include "matLib.h"
#include "cgLib.h"

/**
* Programa Principal
* Exemplo de invocacao : ./cgSolver  -n 100 -k 97 -p 1.2 -i 10000 -e 0.01 -o out.txt
*/
int main(int argc, char **argv){
  double w, e;
  int n, k, i;
  char *outputFileName;

  if(!validaEntradas(argc, argv, &n, &k, &i, &w, &e, &outputFileName)) return 0;
  
  srand(20182);
  double **ABand;
  double **A = constroiMat(n, n);
  double *B  = constroiVetor(n);
  double *X  = constroiVetorZero(n);

  for(int j = 0; j < n; j++) for(int jj = 0; jj < n; jj++) A[j][jj] = generateRandomA(j,jj,k);
  for(int j = 0; j < n; j++) B[j] = generateRandomB(k);
  matToMatSym(A, n, k);
  ABand = constroiBandWidthMatSym(A, n, k);

  FILE *fp = fopen(outputFileName, "w");
  fprintf(fp, "# login1 phcs09 \n");fprintf(fp, "# login2 phcs09 \n#\n");
  double erro = gradienteConjugadoBand(ABand, &X, B, n, k, i, w, e, fp);
  fclose(fp);
  return 0;
}
