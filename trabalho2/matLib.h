#ifndef __MATLIB_H__
#define __MATLIB_H__

/* Construtores de Arrays e Matrizes */
double *constroiVetor(int n);
double *constroiVetorZero(int n);
double **constroiMat(int l, int c);
double **constroiBandWidthMat(double **A, int N, int k);
double **constroiBandWidthMatSym(double **A, int n, int k);
double **constroiMatIdent(int l, int c);
void destroiMat(double **M, int l, int c);
void copiaArray(double *A, double *B, int n);
void copiaMatriz(double **A, double **B, int n);

/* Metodos auxiliares */
void mostraArray(double *B, int n);
void mostraMat(double  **A, int l, int c);

/* Somas */
double *somaArray(double *B, double *C, int N);
double *subtraiArray(double *B, double *C, int N);
double normaEuclidiana(double *A, int N);
double normaMax(double *A, int N);

/*Diagonal Dominante*/
int diagonalDominante(double **A, int n);

/* Matrizes Triangular L e U */
double **triangularL(double **M, int N);
double **triangularU(double **M, int N);
double **triangularLBand(double ** M, int n, int k);
double **triangularUBand(double ** M, int n, int k);

/* Matriz Diagonal D */
double **diagonalD(double **M, int N);
double **diagonalDBand(double ** M, int n, int k);

/*Eliminacoes*/
double **eliminacaoGauss(double **A, int n, double **L);
double **eliminacaoGaussInversa(double **A, int n, double **L);
double **eliminacaoColunas(double **A, int n, double **L);

/*Fatoracoes*/
void lu(double **A, int n, double **L, double **U);
void ldu(double **A, int n, double **L, double **D, double **U);

/* Multiplicacoes */
double *multEscalar(double alpha, double *b, int n);
double multEscalarArray(double *a, double *b, int n);

/*determinante*/
double det(double **M, int N);
double detCof(double **M, int N, int cofI, int cofJ);

/* equivalncia, Criterio de linhas , Sassenfeld e simetrica*/
int criterioLinhas(double **A,int l, int c);
int criterioSassenfeld(double **A,int l, int c);
int equivalencia(double **A, double **b, int l, int c);
int simetrica(double **A,int l, int c);

/*transposta*/
double **transposta(double **M, int N);

/* Cofator */
double **cof(double **M, int N);

/* Adjunta */
double **adj(double **M, int N);

/* Inversa */
double **inversa(double **M, int N);
double **inversaEliminacao(double **A, int N);
double **inversaEliminacaoDiag(double **D, int n);

/* Operacoes Matriz */
double **somaMatriz(double **A, double **B, int N);
double **multiplicaMatrizEscalar(double **A, double w, int N);
double **multiplicaMatriz(double **A, double **B, int N);
#endif // __MATLIB_H__