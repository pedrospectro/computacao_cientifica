#ifndef __CGLIB_H__
#define __CGLIB_H__

// Parametros de erro e convergencia de iteracoes
#define MAXIT 500
#define EPS 1.0e-4

/* Contrucao de matriz esparsa aleatoria k-diagonal*/
double generateRandomA(unsigned int i, unsigned int j, unsigned int k);
void matToMatSym(double **m, int n, int k);
double generateRandomB(unsigned int k);

/* Validacao e leitura de parametros */
int trataN(int *n, int value);
int trataI(int *i, int value);
int trataK(int *k, int value);
int trataO(char **outputFileName, char *value);
void trataW(double *w, double value);
void trataE(double *e, double value);
int validaEntradas(int argc, char **argv, int *n, int *k, int *i, double *w, double *e, char **outputFileName);

/*Calcula sitema AX*/
double *ax(double **A, double *X, int N);
double *axBand(double **A, double *X, int N, int k);
double *axBandSym(double **A, double *X, int N, int k);

/* Pré condicionadores Band*/
double **preCondicionadorBandInversa(double **M, int N, int k, double w);
double **preCondicionadorJacobiBand(double **M, int N, int k);
double **preCondicionadorGausSiedelBand(double **M, int N, int k);
double **preCondicionadorSSORBand(double **M, int n, int k, double w);

/* Metodo do gradiente para matriz de banda simetrica
 que usa todos os pre condicionadores definidos no trabalho*/
double gradienteConjugadoBand(double **Aband, double **X, double *B, int n, int k, int i, double w, double e, FILE *fp);

#endif // __CGLIB_H__