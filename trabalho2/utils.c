/**
* @file utils.c
* @brief Biblioteca para calculo do timestamp
*/
#include "utils.h"

/**
* Função que gera um timestamp corrente
*/
double timestamp(void)
{
  struct timeval tp;
  gettimeofday(&tp, NULL);
  return((double)(tp.tv_sec*1000.0 + tp.tv_usec/1000.0));
}