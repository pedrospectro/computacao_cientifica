/**
* @file cgLib.c
* @brief Biblioteca para calculo do Gradiente Conjugado com matriz de banda simetrica
*/
#include <stdio.h>
#include <math.h>
#include <float.h>
#include "utils.h"
#include "matLib.h"
#include "cgLib.h"
#include <string.h>

/**
* Função que gera os termos independentes de um sistema linear k-diagonal
* i,j: coordenadas do elemento a ser calculado (0<=i,j<n)
* k: numero de diagonais da matriz A
*/
inline double generateRandomA( unsigned int i, unsigned int j, unsigned int k )
{
  if(i>j) return 0;
  k = (int) k / 2 ;
  static double invRandMax = 1.0 / (double)RAND_MAX;
  if(abs((int)i - (int)j) > k) return 0.0;
  if (i==j) return (double) (k<<1) * (double)(rand() * invRandMax) + k;
  return (double)(rand() * invRandMax);
}

/**
* Função que transforma A k-diagonal em simetrica
*/
void matToMatSym(double **m, int n, int k)
{
  for(int i = 0; i < n; i++) for(int j = 0; j < n; j++) if(i>j) m[i][j] = m[j][i];
}

/**
* Função que gera os termos independentes de um sistema linear k-diagonal
* k: numero de diagonais da matriz A
*/
inline double generateRandomB( unsigned int k )
{
  static double invRandMax = 1.0 / (double)RAND_MAX;
  return ((double)(k<<2)) * ((double)rand() * invRandMax);
}

int trataN(int *n, int value){
  if(value <= 3) { printf("A dimensão do sistema de Valor N deve ser maior do que 10\n"); return 0; }
  else *n = value; return 1;
}

int trataK(int *k, int value){
  if(value > 1 && value%2==1) { *k = value; return 1; }
  else{ printf("O valor de número de diagonais k deve ser impar e maior que 1\n"); return 0; }
}

int trataI(int *i, int value){ *i = value; return 1; }

int trataO(char **outputFileName, char *value){ *outputFileName = value; return 1; }

void trataW(double *w, double value){ *w = (value >= 0.0) ? value : 0.0; }

void trataE(double *e, double value){ *e = value; }

/**
* Funcao para Validar as entradas de acordo com a especificacao em README.pdf
*/
int validaEntradas(int argc, char **argv, int *n, int *k, int *i, double *w, double *e, char **outputFileName){
  int vN, vK, vI, vO; vN = vK = vI = vO = 0; *e = 0.0;
  for(int j=1; j < argc - 1; j=j+2){
    if(strcmp(argv[j], "-n") == 0) { if(trataN(n, atoi(argv[j + 1]))) vN = 1; }
    else if(strcmp(argv[j], "-k") == 0) { if(trataK(k, atoi(argv[j + 1]))) vK = 1; }
    else if(strcmp(argv[j], "-p") == 0) trataW(w, atof(argv[j + 1]));
    else if(strcmp(argv[j], "-i") == 0) { if(trataI(i, atoi(argv[j + 1]))) vI = 1; }
    else if(strcmp(argv[j], "-e") == 0) trataE(e, atof(argv[j + 1]));
    else if(strcmp(argv[j], "-o") == 0) { if(trataO(outputFileName, argv[j + 1])) vO = 1; }
    else{
      printf("Parâmetros inválidos\n");
      printf("Exemplo de invocação: ./cgSolver  -n 110 -k 109 -p 0.0 -i 100 -e 0.1 -o out.txt\n");
      return 0;
    }
  }
  if(vN == 0 || vK == 0 || vI == 0 || vO == 0){
    printf("Parâmetros inválidos\n");
    printf("Exemplo de invocação: ./cgSolver  -n 110 -k 109 -p 0.0 -i 100 -e 0.1 -o out.txt\n");
    printf("Obrigatórios: { n, k, i, o }\n"); printf("Opcionais: { p, e }\n");
    return 0;
  }
  return 1;
}

/**
* Funcao para Calculo de Multiplicacao A.X
*/
double *ax(double **A, double *X, int N){
  double *r = constroiVetorZero(N);
  for(int i = 0; i < N; i++)
    for(int j = 0; j < N; j++) r[i] += (A[i][j] * X[j]);
  return r;
}

/**
* Funcao para Calculo de Multiplicacao A.X tal que A é uma matriz de banda nao simétrica
*/
double *axBand(double **A, double *X, int N, int k){
  double *r = constroiVetorZero(N);
  int mainDiagonalJIndex = ((int)k/2);
  for(int i = 0; i < N; i++)
    for(int j = 0; j < k ; j++) r[i] += (A[i][j] * X[i + j - mainDiagonalJIndex]);
  return r;
}

/**
* Funcao para Calculo de Multiplicacao A.X tal que A é uma matriz de banda simétrica
*/
// double *axBandSym(double **A, double *X, int N, int k){
//   double *r = constroiVetorZero(N);
//   for(int i = 0; i < N; i++){
//     for(int j = 0; j < k; j++) r[i] += (A[i][j] * X[i + j]);
//     for(int jj = 1; jj < k && i-jj>=0; jj++) r[i] += (A[i - jj][jj] * X[i - jj]);
//   }
//   return r;
// }

double *axBandSym(double **A, double *X, int N, int k){
  double *r = constroiVetorZero(N);
  for(int i = 0; i < N; i++){
    for(int j = 0; j < k; j++){
      r[i] += (A[i][j] * X[i + j]);
      if((j + 1) < k && (i - (j + 1)) >= 0) r[i] += (A[i - (j + 1)][(j + 1)] * X[i - (j + 1)]);
    }
  }
  return r;
}

/**
* Funcao para Calculo da inversa do PreCondicionador de uma matriz de banda simetrica Utilizando o parametro p/w especificado
*/
double **preCondicionadorBandInversa(double **M, int N, int k, double w){
  if(w == 0.0) return constroiMatIdent(N, N);
  else if(w < 1.0) return inversaEliminacaoDiag(preCondicionadorJacobiBand(M, N, k),N);
  else if(w == 1.0) return inversaEliminacao(preCondicionadorGausSiedelBand(M, N, k), N);
  else return inversaEliminacao(preCondicionadorSSORBand(M, N, k, w), N);
}

/**
* Funcao para Calculo do PreCondicionador Jacobi de uma Matriz de banda simetrica
*/
double **preCondicionadorJacobiBand(double **M, int N, int k) { return diagonalDBand(M, N, k); }

/**
* Funcao para Calculo do PreCondicionador Gauss-Siedel de uma matriz de banda simetrica
*/
double **preCondicionadorGausSiedelBand(double **M, int N, int k) { return preCondicionadorSSORBand(M, N, k, 1.0); }

/**
* Funcao para Calculo do PreCondicionador SSOR de uma matriz de banda simetrica
*/
double **preCondicionadorSSORBand(double **M, int n, int k, double w){
  double **L = triangularLBand(M, n, k);
  double **D = diagonalDBand(M, n, k);
  double **U = triangularUBand(M, n, k);
  double **DInv = inversaEliminacaoDiag(D, n);
  double **wL = multiplicaMatrizEscalar(L, w, n);
  double **wU = multiplicaMatrizEscalar(U, w, n);
  double **DSomaWL = somaMatriz(D, wL, n);
  double **DSomaWU = somaMatriz(D, wU, n);
  return multiplicaMatriz(
    multiplicaMatriz(DSomaWL, DInv, n),
    DSomaWU,
    n
  );
}

/**
* Funcao para Calculo do Gradiente Conjugado de uma matriz de banda simetrica
*/
double gradienteConjugadoBand(double **Aband, double **X, double *B, int n, int k, int i, double w, double e, FILE *fp){
  double t0 = timestamp();
  double **M = preCondicionadorBandInversa(Aband, n, k, w);
  double tempoPreCond = timestamp() - t0;
  double *Ap;
  double ak, pAp;
  double *p = constroiVetorZero(n);
  double rtzNew;
  double *r = subtraiArray(B, axBandSym(Aband, *X, n, k), n);
  double *z = ax(M, r, n);
  copiaArray(z, p, n);
  double *XNew = constroiVetorZero(n);
  double rtz;
  int count;
  double tempoResiduo = 0;
  double t0It = timestamp();
  double bk;
  double norma;
  for(count = 1 ; count <= i; count++){
    double t0Residuo = timestamp();
    rtz = multEscalarArray(r, z, n);
    Ap = axBandSym(Aband, p, n, k);
    pAp = multEscalarArray(p, Ap, n);
    ak = rtz / pAp;
    copiaArray(*X, XNew, n);
    *X = somaArray(*X, multEscalar(ak, p, n), n);
    norma = normaMax(subtraiArray(*X, XNew, n), n);
    r = subtraiArray(r, multEscalar(ak, Ap, n), n);
    tempoResiduo += (timestamp() - t0Residuo);
    z = ax(M, r, n);
    rtzNew = multEscalarArray(z, r, n);
    bk = (rtzNew / rtz);
    fprintf(fp, "# iter %d: <||%.15g||>\n", count, norma);
    p = somaArray(z, multEscalar(bk, p, n), n);
    if(norma < e) break;
  }
  double tempoIter = (timestamp() - t0It) / count;
  tempoResiduo = tempoResiduo / count;
  fprintf(fp, "# residuo: <||%.15g||>\n", normaEuclidiana(r,n));
  fprintf(fp, "# Tempo PC: %lf\n", tempoPreCond/1000);
  fprintf(fp, "# Tempo iter: %lf\n", tempoIter/1000);
  fprintf(fp, "# Tempo residuo: %lf\n#\nn\n", tempoResiduo/1000);
  for(int i=0;i<n;i++) fprintf(fp, "%.15g ", (*X)[i]);
  return normaEuclidiana(r,n);
}
