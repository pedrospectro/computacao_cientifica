#include <stdio.h>
#include "multMat.h"
#include <math.h>

int main(){
  int i, j, N, b, NB;
  double **A, **B, **C;

  scanf("%d", &N); scanf("%d", &b);
  NB = ceil((double)N/b);

  A = constroiMat(N, N); B = constroiMat(N, N);
  for(i = 0; i < N; i++) for(j = 0; j < N; j++) scanf("%lf", &A[i][j]);
  for(i = 0; i < N; i++) for(j = 0; j < N; j++) scanf("%lf", &B[i][j]);

  //printf("N = %d\n", N); printf("b = %d\n", b); printf("NB = %d\n", NB);

  //mostraMat(A,N,N); mostraMat(B,N,N);

  // C = constroiMat(N, N);
  // printf("Multipicacao com blocos MY: (Tn-t0) = %lf, C:\n", multiplicaMatrizBlocoMy(A, B, C, N, N, b, NB));
  // mostraMat(C, N, N);
  
  // C = constroiMat(N, N);
  // printf("Multipicacao com blocos: (Tn-t0) = %lf, C:\n", multiplicaMatrizBloco(A, B, C, N, N, b, NB));
  // mostraMat(C, N, N);
  C = constroiMat(N, N);
  multiplicaMatrizBlocoMy(A, B, C, N, N, b, NB);
  mostraMat(C, N, N);
  return 0;
}
