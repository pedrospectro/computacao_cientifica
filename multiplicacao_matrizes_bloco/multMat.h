#ifndef __MULTMAT_H__
#define __MULTMAT_H__

double *constroiVetor(int n);
double *constroiVetorZero(int n);
double **constroiMat(int l, int c);

/* Metodos Auxiliares */
void mostraArray(double *array, int n);
void mostraMat(double **A, int l, int c);
double multiplicaMatriz(double **A, double **B, double **C, int l, int c);
double multiplicaMatrizBloco(double **A, double **B, double **C, int l, int c, int b, int NB);
double multiplicaMatrizBlocoMy(double **A, double **B, double **C, int l, int c, int b, int NB);

#endif // __MULTMAT_H__