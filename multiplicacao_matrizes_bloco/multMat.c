#include <stdio.h>
#include <math.h>
#include "utils.h"
#include "multMat.h"

/* Metodos Construtores */
double *constroiVetor(int n){ return (double *)malloc(sizeof(double) * n); }

double *constroiVetorZero(int n){ return (double *)calloc(sizeof(double), n); }

double **constroiMat(int l, int c){
  double **a = (double **) malloc(sizeof(double *) * l);
  for(int i = 0; i < l; i++) a[i] = constroiVetorZero(c);
  return a;
}

/* Metodos Auxiliares */
void mostraArray(double *array, int n){
  for(int i = 0; i < n; i++) printf("%lf ", array[i]);
  printf("\n"); 
}

void mostraMat(double **A, int l, int c){
  printf("l = %d, c = %d\n", l, c);
  for(int i = 0; i < l; i++) mostraArray(A[i], c); 
}

double multiplicaMatriz(double **A, double **B, double **C, int l, int c){
  double t0 = timestamp();

  for(int i = 0; i < l; i++)
    for(int j = 0; j < c; j++)
      for(int k = 0; k < c; k++) C[i][j] += (A[i][k]*B[k][j]);

  return (timestamp() - t0);  
}

double multiplicaMatrizBloco(double **A, double **B, double **C, int l, int c, int b, int NB){
  int indI, indJ;
  double t0 = timestamp();

  for(int jj = 0; jj < l; jj += b)
    for(int kk = 0; kk < l; kk += b)
      for(int i = 0; i < l; i++)
        for(int j = jj; j < ((jj + b) > l? l : (jj + b)); j++)
          for(int k = kk; k < ((kk + b) > l ? l : (kk + b)); k++)
            C[i][j] += (A[i][k] * B[k][j]);

  return (timestamp() - t0); 
}

double multiplicaMatrizBlocoMy(double **A, double **B, double **C, int l, int c, int b, int NB){
  double aux;
  int indIA, indJA, indIB, indJB;
  double t0 = timestamp();

  for(int bi = 0; bi < NB; bi++){
    for(int bj = 0; bj < NB; bj++){
      for(int bk = 0; bk < NB; bk++){
        indIA = b*bi; indJA = b*bk; indIB = b*bk; indJB = b*bj;
        for(int i = indIA; i < (indIA + b)&&(i < l); i++)
          for(int j = indJB; j < (indJB + b)&&(j < c); j++)
            for(int k=indJA; k < (indJA+b)&&(k < c && k < l); k++)
              C[i][j] += A[i][k]*B[k][j];
      }
    }
  }

  return (timestamp() - t0); 
}
