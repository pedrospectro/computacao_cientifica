#include <stdio.h>
#include <math.h>
#include <float.h>
#include "utils.h"
#include "ZeroFuncao.h"

#define MINR 1.5
#define MAXR 1.7

int main(int argc, char **argv)
{
  if(argc<2){
    printf("Voce deve informar o grau do polinomio com seus coeficientes\n");
    printf("exemplo de execucao para f(x) = x^3 - 9x^2 + 3.8197 = ./labZero 3 1 -9 0 3.8197\n");
    return 0;
  }
  
  int grau = atoi(argv[1]);
  if(grau<1) { printf("Utilize polinomio de grau >= 1\n"); return 0; }
  if((argc-2)<grau+1){ printf("Quantidades de coeficientes informados incorretos\n"); return 0; }

  int it = 0; double raiz, fx, dfx; 
  double *p = (double *)malloc(sizeof(double)*(grau+1));
  for(int i = grau; i>=0; i--) p[i] = atof(argv[grau-i+2]);

  double t0, tn;
  Polinomio px = criaPolinomio(p, grau);

  t0 = timestamp();
  double erro_biss = bisseccao(px, MINR, MAXR, 0.02, &it, &raiz);
  tn = timestamp();
  calcPolinomio_rapido(px, raiz, &fx, &dfx);
  printf("\nResultado do metodo de bissecao:\n");
  printf(
    "  x = %.20f, erro = %.20f, fx = %.20f, it=%d, tTotal = %lf\n",
    raiz,
    erro_biss,
    fx,
    it,
    (tn-t0)
  );

  t0 = timestamp();
  it = 0;
  double erro_newton = newtonRaphson(px, MINR, 0.02, &it, &raiz);
  tn = timestamp();
  calcPolinomio_rapido(px, raiz, &fx, &dfx);
  printf("\nResultado do metodo de newton:\n");
  printf(
    "  x = %.20f, erro = %.20f, fx = %.20f, it=%d, tTotal = %lf\n",
    raiz,
    erro_newton,
    fx,
    it,
    (tn-t0)
  );

  t0 = timestamp();
  it = 0;
  double erro_secante = secante(px, MINR, MAXR, 0.02, &it, &raiz);
  tn = timestamp();
  calcPolinomio_rapido(px, raiz, &fx, &dfx);
  printf("\nResultado do metodo da secante:\n");
  printf(
    "  x = %.20f, erro = %.20f, fx = %.20f, it=%d, tTotal = %lf\n",
    raiz,
    erro_secante,
    fx,
    it,
    (tn-t0)
  );

  return 0;
}