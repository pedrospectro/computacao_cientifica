#include <stdio.h>
#include <math.h>
#include <float.h>
#include "utils.h"
#include "ZeroFuncao.h"

Polinomio criaPolinomio(double *pp, int grau){
  Polinomio p; p.p = pp; p.grau = grau;
  return p;
}

double bisseccao(Polinomio p, double a, double b, double eps, int *it, double *raiz){
  double m;
  double old_m;
  double erroRelativo, fa, fb, dfa, dfb, fm, dfm;
  do {
    *it=(*it)+1;
    old_m = m; 
    m = (a + b) / 2;
    if((*it)==1) erroRelativo = 100;
    else erroRelativo = fabs(fabs(m - old_m) / fabs(m)) * 100;

    calcPolinomio_rapido(p, a, &fa, &dfa);
    calcPolinomio_rapido(p, b, &fb, &dfb);
    calcPolinomio_rapido(p, m, &fm, &dfm);
    
    if((fa*fm)<0) b = m;
    else a = m;
  } while((erroRelativo > eps) && (*it<MAXIT));
  if(fabs(fm)>eps) erroRelativo = 100;
  else *raiz = m;
  return erroRelativo;
}


double newtonRaphson(Polinomio p, double x0, double eps, int *it, double *raiz){
  double erroRelativo, fx0, dfx0, xi;
  do {
    *it=(*it)+1;
    calcPolinomio_rapido(p, x0, &fx0, &dfx0);
    xi = x0 - (fx0/dfx0);
    erroRelativo = fabs((xi-x0)/xi);
    x0 = xi;
  } while((erroRelativo > eps) && (*it<MAXIT));
  *raiz = xi;
  return erroRelativo;
}


double secante(Polinomio p, double x0, double x1, double eps, int *it, double *raiz){
  double xi;
  double erroRelativo, fx1, fx0, dfx0, dfx1;
  do {
    *it=(*it)+1;
    calcPolinomio_rapido(p, x0, &fx0, &dfx0);
    calcPolinomio_rapido(p, x1, &fx1, &dfx1);
    xi = x1 - (fx1*(x1-x0)/(fx1-fx0));
    erroRelativo = fabs(xi-x1);
    x0 = x1; x1 = xi;
  } while((erroRelativo > eps) && (*it<MAXIT));
  *raiz = xi;
  return erroRelativo;
}

void calcPolinomio_lento(Polinomio p, double x, double *px, double *dpx){
  double fx, dfx;
  fx = 0;
  for(int i = (p.grau); i >= 0; i--) fx += p.p[i] * pow(x,i);
  *px = fx;
  dfx = 0;
  for(int i = (p.grau); i > 0; i--) dfx += p.p[i] * i * pow(x,i-1);
  *dpx = dfx;
}

void calcPolinomio_rapido(Polinomio p, double x, double *px, double *dpx){
  double b = p.p[p.grau], c = b;
  for (int i = (p.grau - 1); i; --i) { b = p.p[i] + b * x; c = b + c * x; }
  b = p.p[0] + b * x;
  *px = b;
  *dpx = c;
}
