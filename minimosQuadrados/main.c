#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <float.h>

double *constroiVetor(int n){ return (double *)calloc(sizeof(double), n); }

double **constroiMat(int l, int c){
  double **a = (double **) malloc(sizeof(double *) * l);
  for(int i = 0; i < l; i++) a[i] = constroiVetor(c);
  return a;
}

/**
* Função que mostra o conteudo de um array
*/
void mostraArray(double *array, int n){
  for(int i = 0; i < n; i++) printf("%lf ", array[i]);
  printf("\n"); 
}

/**
* Função que mostra o conteudo de uma matriz
*/
void mostraMat(double **A, int l, int c){
  if(A == NULL) { printf("Matriz nao existe\n"); return; }
  printf("\n l = %d, c = %d\n", l, c);
  for(int i = 0; i < l; i++) mostraArray(A[i], c); 
}

/*
 xy: vetor bidimensional com p elementos, contendo sequência de pares
 [xi,yi]
 ATA: matriz de tamanho n+1 x n+1 para os coeficientes do S.L.
 (previamente inicializado em 0)
 ATY: matriz de tamanho n+1 x 1 para os termos independentes do S.L.
 (previamente inicializado em 0)
 p: número de pontos
 n: grau do polinômio
*/
void calculaATAeATY(double **xy, double **ATA, double *ATY, int p, int n){
  ATA[0][0] = (double) p; // primeiro valor de ATA é p

  // Laço sobre 'p' é o mais externo porque este eh o vetor com mais
  // elementos. Desta forma minimizamos o cache miss.
  for (int i=0; i < p; ++i) {
    double x = xy[i][0]; double y = xy[i][1]; double powX = 1.0; ATY[0] += y;
    // Calcula primeira linha de ATA e o vetor ATY
    for (int j = 1; j <= n; ++j) {
      powX *= x;
      ATA[0][j] += powX;
      ATY[j] += powX * y;
    }
    // Calcula a ultima coluna de ATA
    for (int j = 1; j <= n; ++j) {
      powX *= x;
      ATA[j][n] += powX;
    }
  }
  // preenche os espaços restantes da matriz ATA
  for (int i=1; i<=n; ++i) for (int j=0; j<n; ++j) ATA[i][j] = ATA[i-1][j+1];
}
int main(){
  int p = 11;
  double **xy = constroiMat(p, 2);
  xy[0][0] = 1872; xy[0][1] = 9.9;
  xy[1][0] = 1890; xy[1][1] = 14.3;
  xy[2][0] = 1900; xy[2][1] = 17.4;
  xy[3][0] = 1920; xy[3][1] = 30.6;
  xy[4][0] = 1940; xy[4][1] = 41.2;
  xy[5][0] = 1950; xy[5][1] = 51.9;
  xy[6][0] = 1960; xy[6][1] = 70.2;
  xy[7][0] = 1970; xy[7][1] = 93.1;
  xy[8][0] = 1980; xy[8][1] = 119.0;
  xy[9][0] = 1991; xy[9][1] = 146.2;
  xy[10][0] = 1996; xy[10][1] = 157.1;

  int n = 2;
  double **ATA = constroiMat(n+1, n+1);
  double *ATY = constroiVetor(n+1);
  calculaATAeATY(xy, ATA, ATY, p, n);

  printf("Pontos:\n");
  mostraMat(xy,p,2);
  printf("\nATA:\n");
  mostraMat(ATA, n+1, n+1);
  printf("\nATY:\n");
  mostraArray(ATY, n+1);

  return 0;
}